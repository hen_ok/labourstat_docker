/**
 * NOTE: This class is auto generated by the swagger code generator program (unset).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package generated.api;

import generated.api.model.EmployeeResponse;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-07-01T15:30:15.924-04:00")

@Api(value = "rest", description = "the rest API")
public interface RestApi {

    @ApiOperation(value = "Persist and show All the Employees", notes = "", response = EmployeeResponse.class, tags={ "employee-controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 100, message = "100 is the message", response = Void.class),
        @ApiResponse(code = 200, message = "Successful Hello World", response = EmployeeResponse.class),
        @ApiResponse(code = 201, message = "Created", response = Void.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Void.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Void.class),
        @ApiResponse(code = 404, message = "Not Found", response = Void.class) })
    
    @RequestMapping(value = "/rest/allEmployees",
        produces = { "application/json", "application/xml" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<EmployeeResponse> allEmployeesMethodUsingPOST();


    @ApiOperation(value = "A single Employee", notes = "", response = EmployeeResponse.class, tags={ "employee-controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 100, message = "100 is the message", response = Void.class),
        @ApiResponse(code = 200, message = "Successful Hello World", response = EmployeeResponse.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Void.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Void.class),
        @ApiResponse(code = 404, message = "Not Found", response = Void.class) })
    
    @RequestMapping(value = "/rest/getOneEmployees/{id}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.GET)
    ResponseEntity<EmployeeResponse> getAllEmployeesMethodUsingGET(@ApiParam(value = "This is the employee Id",required=true ) @PathVariable("id") String id, @NotNull@ApiParam(value = "First name of the employee", required = true) @RequestParam(value = "firstName", required = true) String firstName,@ApiParam(value = "lastName") @RequestParam(value = "lastName", required = false) String lastName);

}
